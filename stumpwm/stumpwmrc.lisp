(asdf:load-system :stumpwm)

(in-package :stumpwm)

(load-module "battery-portable")
(load-module "cpu")
(load-module "mem")
(load-module "net")
(load-module "pass")
(load-module "pinentry")

(ql:quickload :slynk)
(slynk:create-server :dont-close t
                     :port 4008)

(defun file-to-string (file)
  (with-open-file (stream file)
    (let ((contents (make-string (file-length stream))))
      (read-sequence contents stream)
      contents)))

;; commands
(defcommand launch-discord () ()
    (if (search "Guix" (file-to-string "/etc/os-release"))
        (run-shell-command "LD_LIBRARY_PATH='/run/current-system/profile/lib/:/home/n1x/.nix-profile/opt/Discord/' nixGLIntel Discord")
        (run-shell-command "discord")))

(defcommand launch-email () ()
  (run-shell-command "thunderbird"))

(defcommand launch-nyxt () ()
  (run-shell-command "~/.guix-profile/bin/nyxt"))

(defcommand launch-syncthing () ()
  (run-shell-command "syncthing -no-browser"))

(defcommand launch-term () ()
  (run-shell-command "kitty"))

(defcommand emacs () ()
  (run-shell-command "~/.nix-profile/bin/emacs"))

(defcommand emacsclient () ()
  (run-shell-command "~/.nix-profile/bin/emacsclient -c"))

(defcommand emacs-serv () ()
  (run-shell-command "~/.nix-profile/bin/emacs --fg-daemon"))

(defcommand org-capture () ()
  (run-shell-command "~/.nix-profile/bin/emacsclient -c -F '((name . \"doom-capture\") (width . 70) (height . 25) (transient . t))' -e \"(+org-capture/open-frame \"$str\" ${key:-nil})\""))

;; macros
(defun defkeys (keymap keylist)
  (loop for x in keylist
        collect `(define-key keymap (kbd ,(car x)) ,(cdr x))))

;; modeline config
(toggle-mode-line (stumpwm:current-screen)
                  (stumpwm:current-head))

(setf
 *mode-line-background-color* "#000000"
 *mode-line-foreground-color* "#e60073"
 *mode-line-border-color* "#000000"
 *mode-line-border-width* 0
 *mode-line-timeout* 1
 *time-modeline-string* "%H:%M"
 *window-format* "<%n:%20t>"
 *group-format* " %t "
 *screen-mode-line-format*
 (list "[^B%n^b] |"
       " %d |"
       " %W "
       "^>"
       " %M |"
       " %C |"
       " %B"))

(set-focus-color "#701543")
(set-fg-color "#e60073")
(set-border-color "#e60073")
(set-font "IBM3270")

(set-prefix-key (kbd "s-w"))

(setf *mouse-focus-policy* :click)

(define-key *top-map* (kbd "C-;") "colon")

(defvar *app-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "d") "launch-discord")
    (define-key m (kbd "p") "pass-copy-menu")
    (define-key m (kbd "n") "launch-nyxt")
    (define-key m (kbd "e") "launch-email")
    (define-key m (kbd "s") "launch-syncthing")
    m))

(defvar *emacs-bindings*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "e") "emacsclient")
    (define-key m (kbd "E") "emacs")
    (define-key m (kbd "c") "org-capture")
    (define-key m (kbd "s") "emacs-serv")
    m))

(define-key *top-map* (kbd "s-a") '*app-bindings*)

(define-key *top-map* (kbd "s-e") '*emacs-bindings*)

(define-key *top-map* (kbd "s-RET") "launch-term")

(defun default-groups (grouplist)
  (grename (first grouplist))
  (map 'list (lambda (x) (gnewbg x)) (rest grouplist)))

(defvar *group-names* '("((-P)):"
                        "(-P):"
                        ":"
                        "(:)"
                        "::"
                        "((:))"
                        ":(:)"
                        "(::)"
                        ":::"
                        "(:)(:)") )

(defun startup-hooks ()
  (default-groups *group-names*)
  (run-shell-command "feh --bg-fill ~/code/dotfiles/razorgrrl/wallpaper2.jpg")
  (run-shell-command "~/.nix-profile/bin/emacs --daemon"))

(add-hook *start-hook* 'startup-hooks)

;; volume control
(defun gack/display-current-volume ()
  (message "Volume: ~A[~A%]"
           (if (equal (run-shell-command "pamixer --get-mute" t)
                      (format nil "true~%"))
               "MUTE "
               "")
           (string-trim '(#\newline) (run-shell-command "pamixer --get-volume" t))))

(defcommand gack/change-vol (vol force)
    ((:number "A positive or negative number: ")
     (:y-or-n "override volume limits"))
  (let ((paopt (concat (if (< vol 0)
                           (format nil "--decrease ~D" (* vol -1))
                           (format nil "--increase ~D" vol))
                       (if force
                           (format nil " --allow-boost")
                           ""))))
    (run-shell-command (format nil "pamixer ~A" paopt))
    (gack/display-current-volume)))

(defcommand gack/vol-mute () ()
  (run-shell-command "pamixer -t")
  (gack/display-current-volume))

(defvar keys/volume (defkeys *top-map*
                        '(("XF86AudioLowerVolume" "gack/change-vol -5 n")
                          ("XF86AudioRaiseVolume" "gack/change-vol 5 n")
                          ("C-XF86AudioLowerVolume" "gack/change-vol -1 n")
                          ("C-XF86AudioRaiseVolume" "gack/change-vol 1 n")
                          ("S-XF86AudioLowerVolume" "gack/change-vol -25 n")
                          ("S-XF86AudioRaiseVolume" "gack/change-vol 25 n")

                          ;; outside bounds
                          ("M-XF86AudioLowerVolume" "gack/change-vol -5 y")
                          ("M-XF86AudioRaiseVolume" "gack/change-vol 5 y")
                          ("M-C-XF86AudioLowerVolume" "gack/change-vol -1 y")
                          ("M-C-XF86AudioRaiseVolume" "gack/change-vol 1 y")
                          ("M-S-XF86AudioLowerVolume" "gack/change-vol -25 y")
                          ("M-S-XF86AudioRaiseVolume" "gack/change-vol 25 y")

                          ;; mute
                          ("XF86AudioMute" "gack/vol-mute")
                          )))

;;; Within bounds
  (define-key *top-map* (kbd "XF86AudioLowerVolume") "gack/change-vol -5 n")
  (define-key *top-map* (kbd "XF86AudioRaiseVolume") "gack/change-vol 5 n")
  (define-key *top-map* (kbd "C-XF86AudioLowerVolume") "gack/change-vol -1 n")
  (define-key *top-map* (kbd "C-XF86AudioRaiseVolume") "gack/change-vol 1 n")
  (define-key *top-map* (kbd "S-XF86AudioLowerVolume") "gack/change-vol -25 n")
  (define-key *top-map* (kbd "S-XF86AudioRaiseVolume") "gack/change-vol 25 n")

;;; Outside bounds
  (define-key *top-map* (kbd "M-XF86AudioLowerVolume") "gack/change-vol -5 y")
  (define-key *top-map* (kbd "M-XF86AudioRaiseVolume") "gack/change-vol 5 y")
  (define-key *top-map* (kbd "M-C-XF86AudioLowerVolume") "gack/change-vol -1 y")
  (define-key *top-map* (kbd "M-C-XF86AudioRaiseVolume") "gack/change-vol 1 y")
  (define-key *top-map* (kbd "M-S-XF86AudioLowerVolume") "gack/change-vol -25 y")
  (define-key *top-map* (kbd "M-S-XF86AudioRaiseVolume") "gack/change-vol 25 y")

;;; Muting
  (define-key *top-map* (kbd "XF86AudioMute") "gack/vol-mute")
