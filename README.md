# 𝖌𝖆𝖈𝖐

A collection of themes for GTK (+ icons), Xresources, emacs, and slack based off
my gack pallette. 

# Acknowledgements
Thanks to [oomox](https://github.com/themix-project/oomox) for making themeing
GTK easy, sixsixfive for their awesome and re-colorable [DarK icon
set](https://gitlab.com/sixsixfive/dark-icons/), mswift42 for their [theme
creator](https://github.com/mswift42/themecreator)
