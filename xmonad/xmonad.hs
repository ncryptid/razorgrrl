{-# LANGUAGE FlexibleContexts #-}
import XMonad
import XMonad.Util.EZConfig
import XMonad.Util.Run
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.FadeInactive
import XMonad.Util.Themes
import XMonad.Layout.LayoutModifier(LayoutModifier(handleMess, modifyLayout,
                                    redoLayout),
                                    ModifiedLayout(..))
import XMonad.Layout.Simplest(Simplest(..))
import XMonad.Layout.Decoration(Decoration, DefaultShrinker)
import qualified XMonad as X
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation
import XMonad.Layout.Tabbed
import XMonad.Layout.BoringWindows
import XMonad.Actions.Navigation2D
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps
import XMonad.Layout.NoBorders
import qualified XMonad.StackSet as W
import System.IO

-- Define some base variables
myWorkspaces         = ["((-P)):","(-P):",":","(:)","::","((:))",":(:)","(::)",":::","(:)(:)"]
myTerminal           = "alacritty"
myModMask            = mod4Mask
myBorderWidth        = 1
myBorderColor        = "#e60073"
myNormalBorderColor  = "#94408b"
myTabConfig = def { inactiveBorderColor = "#94408b"
                  , inactiveTextColor = "#94408b"
                  , inactiveColor = "#000000"
                  , activeBorderColor = "#e60073"
                  , activeTextColor = "#e60073"
                  , activeColor = "#000000"
                  , fontName = "inconsolata"}

-- Custom sublayout implementation
-- mySubTabbed :: (Eq a, LayoutModifier (Sublayout Simplest) a, LayoutClass l a) =>
--     l a -> ModifiedLayout (Decoration TabbedDecoration Shrinker)
--                           (ModifiedLayout (Sublayout Simplest) l) a
-- mySubTabbed  x = addTabs shrinkText (theme myTabConfig) X.def $ subLayout [] Simplest x

-- Define startup stuff
myStartupHook = do
  spawn "feh --bg-fill ~/code/dotfiles/razorgrrl/wallpaper2.jpg"
  spawn "syncthing --no-browser"

-- Main function

main :: IO ()
main = do
  xmproc <- spawnPipe "xmobar"
  xmonad
    $ defaultConfig
    { terminal           = myTerminal
    , modMask            = myModMask
--    , workspaces         = myWorkspaces
    , borderWidth        = myBorderWidth
    , focusedBorderColor = myBorderColor
    , normalBorderColor  = myNormalBorderColor
    , startupHook        = myStartupHook
    , layoutHook         = smartBorders $
                           avoidStruts $
                           windowNavigation $
                           subTabbed $
                           boringWindows $
                           emptyBSP
                           ||| Full
    } `additionalKeysP`
    [ ("M-<Return>",               spawn "WINIT_X11_SCALE_FACTOR=1 alacritty")                        -- Open the terminal
    -- Window management
    , ("M-g",                      windows W.swapMaster)                                          -- Swap the focused window and the master window
    , ("M-.",                      sendMessage (IncMasterN 1))                                    -- Increment master windows
    , ("M-,",                      sendMessage (IncMasterN (-1)))                                 -- Decrement master windows
    , ("M-M1-<Right>",             sendMessage $ ShrinkFrom L)
    , ("M-M1-<Up>",                sendMessage $ ExpandTowards U)
    , ("M-M1-<Down>",              sendMessage $ ShrinkFrom U)
    , ("M-M1-C-<Left>",            sendMessage $ ShrinkFrom R)
    , ("M-M1-C-<Right>",           sendMessage $ ExpandTowards R)
    , ("M-M1-C-<Up>",              sendMessage $ ShrinkFrom D)
    , ("M-M1-C-<Down>",            sendMessage $ ExpandTowards D)
    , ("M-s",                      sendMessage $ XMonad.Layout.BinarySpacePartition.Swap)
    , ("M-M1-s",                   sendMessage $ Rotate)
    -- Tabbed sublayout
    , ("M-C-h",                    sendMessage $ pullGroup L)
    , ("M-C-l",                    sendMessage $ pullGroup R)
    , ("M-C-k",                    sendMessage $ pullGroup U)
    , ("M-C-j",                    sendMessage $ pullGroup D)
    , ("M-C-m",                    withFocused (sendMessage . MergeAll))
    , ("M-C-u",                    withFocused (sendMessage . UnMerge))
    , ("M-M1-C-u",                 withFocused (sendMessage . UnMergeAll))
    , ("M-j",                      focusDown)
    , ("M-k",                      focusUp)
    -- Application shortcuts
    , ("M-d",                      spawn "rofi -show run")                                        -- Open the launcher
    , ("M-C-s",                    spawn "flameshot gui")                                         -- Screenshot selection
--    , ("M-S-n",                    spawn "flameshot full --path ~/Cloud/Pictures/Downloads")      -- Full screenshot
--    , ("M-C-o",                    spawn "/home/n1x/.emacs.d/bin/org-capture")                    -- org-capture
    , ("M-e",                      spawn "emacsclient -c")
    , ("M-c",                      kill)                                                          -- Close the focused window
    , ("M-S-r",                    broadcastMessage ReleaseResources >> restart "xmonad" True)    -- Restart xmonad
    -- Hardware stuff
    , ("<XF86AudioRaiseVolume>",   spawn "pulseaudio-ctl up 2")                                   -- Raise volume
    , ("<XF86AudioLowerVolume>",   spawn "pulseaudio-ctl down 2")                                 -- Lower volume
    , ("<XF86AudioMute>",          spawn "pulseaudio-ctl mute")                                   -- Mute
    , ("<XF86MonBrightnessUp>",    spawn "light -A 5")                                            -- Raise brightness
    , ("<XF86MonBrightnessDown>",  spawn "light -U 5")                                            -- Lower brightness
    ]

-- A copy of the default keybindings for reference
help :: String
help = unlines ["The default modifier key is 'alt'. Default keybindings:",
    "",
    "-- launching and killing programs",
    "mod-Shift-Enter  Launch xterminal",
    "mod-p            Launch dmenu",
    "mod-Shift-p      Launch gmrun",
    "mod-Shift-c      Close/kill the focused window",
    "mod-Space        Rotate through the available layout algorithms",
    "mod-Shift-Space  Reset the layouts on the current workSpace to default",
    "mod-n            Resize/refresh viewed windows to the correct size",
    "",
    "-- move focus up or down the window stack",
    "mod-Tab        Move focus to the next window",
    "mod-Shift-Tab  Move focus to the previous window",
    "mod-j          Move focus to the next window",
    "mod-k          Move focus to the previous window",
    "mod-m          Move focus to the master window",
    "",
    "-- modifying the window order",
    "mod-Return   Swap the focused window and the master window",
    "mod-Shift-j  Swap the focused window with the next window",
    "mod-Shift-k  Swap the focused window with the previous window",
    "",
    "-- resizing the master/slave ratio",
    "mod-h  Shrink the master area",
    "mod-l  Expand the master area",
    "",
    "-- floating layer support",
    "mod-t  Push window back into tiling; unfloat and re-tile it",
    "",
    "-- increase or decrease number of windows in the master area",
    "mod-comma  (mod-,)   Increment the number of windows in the master area",
    "mod-period (mod-.)   Deincrement the number of windows in the master area",
    "",
    "-- quit, or restart",
    "mod-Shift-q  Quit xmonad",
    "mod-q        Restart xmonad",
    "",
    "-- Workspaces & screens",
    "mod-[1..9]         Switch to workSpace N",
    "mod-Shift-[1..9]   Move client to workspace N",
    "mod-{w,e,r}        Switch to physical/Xinerama screens 1, 2, or 3",
    "mod-Shift-{w,e,r}  Move client to screen 1, 2, or 3",
    "",
    "-- Mouse bindings: default actions bound to mouse events",
    "mod-button1  Set the window to floating mode and move by dragging",
    "mod-button2  Raise the window to the top of the stack",
    "mod-button3  Set the window to floating mode and resize by dragging"]
