;;; ../code/dotfiles/razorgrrl/doom-emacs/comms.el -*- lexical-binding: t; -*-
(require 'with-proxy)

(setq circe-network-options
      `(("Freenode"
         :tls t
         :nick "n1x_"
         :sasl-username "n1x_"
         :sasl-password ,(+pass-get-secret "comm/irc/nick"))
        ("Bitlbee"
         :nick "bitlbee"
         :port "6667")))

;; (with-proxy
;;   :http-server "127.0.0.1:8118"
;;   (circe "Freenode"))
