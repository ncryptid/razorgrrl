;;; ../code/dotfiles/razorgrrl/doom-emacs/elget.el -*- lexical-binding: t; -*-

(setq el-get-sources
      '((:name calibre-query
       :type git
       :url "git://github.com/whacked/calibre-query.el.git"
       :features "calibre-query")))
