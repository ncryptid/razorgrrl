;;; keybinds.el -*- lexical-binding: t; -*-


(defun insert-existential ()
  (interactive)
  (insert "∃"))

(defun insert-universal ()
  (interactive)
  (insert "∀"))

(defun insert-and ()
  (interactive)
  (insert "∧"))

(defun insert-or ()
  (interactive)
  (insert "∨"))

(defun insert-not ()
  (interactive)
  (insert "¬"))

(defun insert-if-then ()
  (interactive)
  (insert "⊃"))

(defun insert-iff ()
  (interactive)
  (insert "≡"))

(defun insert-therefore ()
  (interactive)
  (insert "∴"))

;;;; org-mode
(map! :map org-mode-map
      :localleader
      "C"   #'org-toggle-checkbox
      "` e" #'insert-existential
      "` u" #'insert-universal
      "` a" #'insert-and
      "` o" #'insert-or
      "` n" #'insert-not
      "` i" #'insert-if-then
      "` f" #'insert-iff
      "` t" #'insert-therefore)

;; org-noter
(map!
 :map org-noter-doc-mode-map
 "I" #'org-noter-insert-note-toggle-no-questions
 "i" #'org-noter-insert-note
 "q" #'org-noter-kill-session)

(map! :leader
      (:prefix ("j" . "journal") ;; org-journal bindings
       :desc "Create new journal entry" "j" #'org-journal-new-entry
       :desc "Open previous entry" "p" #'org-journal-open-previous-entry
       :desc "Open next entry" "n" #'org-journal-open-next-entry
       :desc "Search journal" "s" #'org-journal-search-forever))

;; The built-in calendar mode mappings for org-journal
;; conflict with evil bindings
(map!
 (:map calendar-mode-map
  :n "o" #'org-journal-display-entry
  :n "p" #'org-journal-previous-entry
  :n "n" #'org-journal-next-entry
  :n "O" #'org-journal-new-date-entry))

;; Local leader (<SPC m>) bindings for org-journal in calendar-mode
(map!
 :map (calendar-mode-map)
 :localleader
 "w" #'org-journal-search-calendar-week
 "m" #'org-journal-search-calendar-month
 "y" #'org-journal-search-calendar-year)


;;;; calibredb
(map! :after calibre-show-mode
      :map calibredb-show-mode-map
      :n "?" #'calibredb-entry-dispatch
      :n "o" #'calibredb-find-file
      :n "O" #'calibredb-find-file-other-frame
      :n "V" #'calibredb-open-file-with-default-tool
      :n "s" #'calibredb-set-metadata-dispatch
      :n "e" #'calibredb-export-dispatch
      :n "q" #'calibredb-entry-quit
      :n "." #'calibredb-open-dired
      :n "\M-t" #'calibredb-set-metadata--tags
      :n "\M-a" #'calibredb-set-metadata--author_sort
      :n "\M-A" #'calibredb-set-metadata--authors
      :n "\M-T" #'calibredb-set-metadata--title
      :n "\M-c" #'calibredb-set-metadata--comments)

(map! :after calibredb
      :map calibredb-search-mode-map
      :n "?" #'calibredb-dispatch
      :n "a" #'calibredb-add
      :n "A" #'calibredb-add-dir
      :n "c" #'calibredb-clone
      :n "d" #'calibredb-remove
      :n "j" #'calibredb-next-entry
      :n "k" #'calibredb-previous-entry
      :n "l" #'calibredb-library-list
      :n "n" #'calibredb-library-next
      :n "p" #'calibredb-library-previous
      :n "s" #'calibredb-set-metadata-dispatch
      :n "S" #'calibredb-switch-library
      :n "o" #'calibredb-find-file
      :n "O" #'calibredb-find-file-other-frame
      :n "v" #'calibredb-view
      :n "V" #'calibredb-open-file-with-default-tool
      :n "." #'calibredb-open-dired
      :n "b" #'calibredb-catalog-bib-dispatch
      :n "e" #'calibredb-export-dispatch
      :n "r" #'calibredb-search-refresh-and-clear-filter
      :n "R" #'calibredb-search-refresh-or-resume
      :n "q" #'calibredb-search-quit
      :n "m" #'calibredb-mark-and-forward
      :n "f" #'calibredb-toggle-favorite-at-point
      :n "x" #'calibredb-toggle-archive-at-point
      :n "h" #'calibredb-toggle-highlight-at-point
      :n "u" #'calibredb-unmark-and-forward
      :n "i" #'calibredb-edit-annotation
      :n (kbd "<DEL>") #'calibredb-unmark-and-backward
      :n (kbd "<backtab>") #'calibredb-toggle-view
      :n (kbd "TAB") #'calibredb-toggle-view-at-point
      :n "\M-n" #'calibredb-show-next-entry
      :n "\M-p" #'calibredb-show-previous-entry
      :n "/" #'calibredb-search-live-filter
      :n "\M-t" #'calibredb-set-metadata--tags
      :n "\M-a" #'calibredb-set-metadata--author_sort
      :n "\M-A" #'calibredb-set-metadata--authors
      :n "\M-T" #'calibredb-set-metadata--title
      :n "\M-c" #'calibredb-set-metadata--comments)



;;;; winum window selection by number
(setq winum-scope 'frame-local)
(map! :leader
      :desc "Select window 0..9" "0" #'winum-select-window-0
      :desc "Select window 0..9" "1" #'winum-select-window-1
      :desc "Select window 0..9" "2" #'winum-select-window-2
      :desc "Select window 0..9" "3" #'winum-select-window-3
      :desc "Select window 0..9" "4" #'winum-select-window-4
      :desc "Select window 0..9" "5" #'winum-select-window-5
      :desc "Select window 0..9" "6" #'winum-select-window-6
      :desc "Select window 0..9" "7" #'winum-select-window-7
      :desc "Select window 0..9" "8" #'winum-select-window-8
      :desc "Select window 0..9" "9" #'winum-select-window-9)

(require 'which-key)

(push '(("\\(.*\\) 0" . "winum-select-window-0") . ("\\1 0..9" . "Select window 0..9"))
      which-key-replacement-alist)
(push '((nil . "winum-select-window-[1-9]") . t) which-key-replacement-alist)
