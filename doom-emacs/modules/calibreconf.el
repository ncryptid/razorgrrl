;;; ../code/dotfiles/razorgrrl/doom-emacs/calibreconf.el -*- lexical-binding: t; -*-

;;;; Calibre settings
(require 'calibredb)
(setq sql-sqlite-program "/run/current-system/sw/bin/sqlite3")
(setq calibredb-root-dir "~/Cloud/ebooks/Calibre")
(setq calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir))
(setq calibredb-program "/run/current-system/sw/bin/calibre")
(setq calibredb-library-alist '((calibredb-root-dir)))
(setq calibredb-format-all-the-icons t)
