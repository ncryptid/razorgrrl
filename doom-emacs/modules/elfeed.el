;;; ../code/dotfiles/razorgrrl/doom-emacs/elfeed.el -*- lexical-binding: t; -*-
(setq elfeed-feeds
      '("https://xenogothic.com/feed/"
        "https://reciprocalcontradiction.home.blog/feed/"
        "https://nyxus.xyz/posts/index.xml"
        "https://memo.barrucadu.co.uk/atom.xml"
        "https://vkjehannum.wordpress.com/feed"
        "https://michaelochurch.wordpress.com/feed"
        "https://drewdevault.com/blog/index.xml"
        ))

(after! elfeed
  (setq elfeed-search-filter "@1-month-ago +unread"))
(add-hook! 'elfeed-search-mode-hook 'elfeed-update)
