;;; common-lisp.el -*- lexical-binding: t; -*-
(add-to-list 'load-path "~/common-lisp/lisp-system-browser")

(require 'sly-autoloads)

(add-to-list 'sly-contribs 'system-browser)
