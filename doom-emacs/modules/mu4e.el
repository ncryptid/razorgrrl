;;; mu4e.el -*- lexical-binding: t; -*-
(setq +mu4e-backend 'offlineimap)
(set-email-account! "riseup"
  '((mu4e-sent-folder       . "/riseup/Sent")
    (mu4e-drafts-folder     . "/riseup/Drafts")
    (mu4e-trash-folder      . "/riseup/Trash")
    (mu4e-refile-folder     . "/riseup/All Mail")
    (smtpmail-smtp-user     . "n1x@riseup.net")
    (mu4e-compose-signature . "---\n-nyx"))
  t)
