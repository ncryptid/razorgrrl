;;; guix.el -*- lexical-binding: t; -*-
;; Some custom functions for guix development

(defun gack/blank-template (n u)
  (interactive
   (list
    (read-string "Package name: ")
    (read-string "Git URL: ")))
  (insert (format "(define-public %s
  (let ((commit \"0000000000000000000000000000000000000000\")
        (revision \"1\"))
  (package
   (name \"%s\")
   (version (git-version \"0.0.0\" revision commit))
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url \"%s\")
           (commit commit)))
     (sha256
      (base32
       \"0000000000000000000000000000000000000000000000000000\"))
     (file-name (git-file-name name version))))
   (build-system )
   (inputs
    `((\"example-dep\" ,example-dep)))
   (synopsis \"Example synopsis\")
   (description
    \"Example description\")
   (home-page \"%s\")
   (license license:)))))" n n u u)))

(defun gack/input-template (n)
  (interactive
   (list
    (read-string "Package name: ")))
  (insert (format "(\"%s\" ,%s)
" n n)))

(defun gack/golang-template (n u)
  (interactive
   (list
    (read-string "Package name: ")
    (read-string "Git URL: ")))
  (insert (format "(define-public go-github-com-%s
  (let ((commit \"0000000000000000000000000000000000000000\")
        (revision \"1\"))
  (package
   (name \"go-github-com-%s\")
   (version (git-version \"0.0.0\" revision commit))
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url \"%s\")
           (commit commit)))
     (sha256
      (base32
       \"0000000000000000000000000000000000000000000000000000\"))))
   (build-system go-build-system)
   (inputs
    `((\"example-dep\" ,example-dep)))
  (arguments
     '(#:import-path \"\"))
   (synopsis \"Example synopsis\")
   (description
    \"Example description\")
   (home-page \"%s\")
   (license license:)))))" n n u u)))


(defun gack/asdf-sbcl-template (n u)
  (interactive
   (list
    (read-string "Package name: ")
    (read-string "Git URL: ")))
  (insert (format "(define-public sbcl-%s
  (let ((commit \"0000000000000000000000000000000000000000\")
        (revision \"1\"))
  (package
   (name \"sbcl-%s\")
   (version (git-version \"0.0.0\" revision commit))
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url \"%s\")
           (commit commit)))
     (sha256
      (base32
       \"0000000000000000000000000000000000000000000000000000\"))
     (file-name (git-file-name name version))))
   (build-system asdf-build-system/sbcl)
   (inputs
    `((\"example-dep\" ,example-dep)))
   (synopsis \"Example synopsis\")
   (description
    \"Example description\")
   (home-page \"%s\")
   (license license:)))))" n n u u)))

(defun gack/asdf-ecl-template (n)
  (interactive
   (list
    (read-string "Package name: ")))
  (insert
   (format "(define-public ecl-%s
  (sbcl-package->ecl-package sbcl-%s))" n n)))

(defun gack/asdf-cl-template (n)
  (interactive
   (list
    (read-string "Package name: ")))
  (insert
   (format "(define-public cl-%s
  (sbcl-package->cl-package sbcl-%s))" n n)))
