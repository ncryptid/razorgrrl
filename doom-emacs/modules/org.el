;;; ../code/dotfiles/razorgrrl/doom-emacs/org.el -*- lexical-binding: t; -*-

;;;; Org-Mode settings
(setq org-directory "~/Cloud/org-sync")
(setq org-agenda-files '("~/Cloud/org-sync"))
(setq org-html-postamble "<hr>
<a href=\"../index.html\">back</a>")
(add-hook 'org-mode-hook 'turn-on-auto-fill)

(setq org-log-done 'time)
;; (add-hook 'org-mode-hook (lambda () (org-autolist-mode)))
(with-eval-after-load 'ox
  (require 'ox-hugo))

(global-visual-line-mode nil)
(setq org-highlight-latex-and-related nil)
(setq org-refile-allow-creating-parent-nodes 'confirm)
(setq org-refile-targets '((org-agenda-files :maxlevel . 20)))
(setq org-noter-always-create-frame nil)
(setq org-noter-kill-frame-at-session-end nil)

;; org-babel settings
(setq org-babel-clojure-backend 'cider)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (lisp . t)
   (clojure . t)))

;; Fix for weird unicode rendering on Ubuntu
(when (not (string= (system-name) "djynxx"))
  (setq org-superstar-headline-bullets-list
        '(
          "🟕"
          "🜃"
          "🜂"
          "🜄"
          "🜁"
          )))

;; org-capture templates
(after! org
(setq org-capture-templates
      '(("b" "black")
        ("bb" "bookmarks" entry (file+olp "black.org"
                                          "Reading"
                                          "Bookmarks"
                                          "Inbox")
         "* %? %i")
        ("bn" "notes" entry (file+olp "black.org"
                                      "Notes")
         "* %? %i")

        ("bj" "journal" entry (file+olp+datetree "black.org"
                                                 "Journal")
         "* %<%H:%M> %?\n%i"
         :prepend t)
        ("p" "pink")
        ("pm" "music" entry (file+olp "pink.org"
                                      "Media"
                                      "Music"
                                      "Backlog"
                                      "Inbox")
         "* TODO %? %i")
        ("pv" "movies" entry (file+olp "pink.org"
                                       "Media"
                                       "Movies"
                                       "Backlog")
         "* TODO %? %i")

        ("l" "lavender")
        ("lo" "occult")
        ("lob" "bookmarks" entry (file+olp "lavender.org"
                                           "Occult"
                                           "Bookmarks")
         "* %? %i")
        ("loj" "journal" entry (file+olp+datetree "lavender.org"
                                                  "Occult"
                                                  "Journal"
                                                  "Misc")
         "* %<%H:%M> %?\n%i"
         :prepend t)
        ("lod" "dream journal" entry (file+olp+datetree "lavender.org"
                                                        "Occult"
                                                        "Journal"
                                                        "Dreams")
         "* %<%H:%M> %?\n%i"
         :prepend t)

        ("lt" "tech")
        ("ltb" "bookmarks" entry (file+olp "lavender.org"
                                           "Tech"
                                           "Bookmarks")
         "* %? %i")
        ("ltb" "ideas" entry (file+olp "lavender.org"
                                       "Tech"
                                       "Programming ideas")
         "* %? %i"))))

;; org-agenda settings
(use-package! org-super-agenda
  :after org-agenda
  :init
  (setq org-agenda-custom-commands
        '(("c" "Super view"
           ((agenda "" ((org-agenda-overriding-header "")
                        (org-super-agenda-groups
                         '((:name "Upcoming events"
                            :and (:scheduled t
                                  :date t
                                  :time-grid t)
                            :order 1)
                           (:discard (:todo "DONE"
                                      :todo "KILL"))))))
            (todo "" ((org-agenda-overriding-header "")
                      (org-super-agenda-groups
                       '((:name "Important"
                                :and (:priority "A" :not (:tag "reading")))
                         (:name "Shopping lists"
                                :tag "shopping")
                         (:name "Reading list"
                                :and (:priority "A" :tag "reading")
                                :order 6)
                         (:name "Movies backlog"
                                :and (:tag "movies" :tag "backlog")
                                :order 7)
                         (:name "Music backlog"
                                :and (:tag "music" :tag "backlog")
                                :order 8)
                         (:discard (:anything t))))))))))

  :config
  (org-super-agenda-mode))

;; ;; org-journal settings
;; (customize-set-variable 'org-journal-dir "~/Cloud/org-sync/.violet")
;; (customize-set-variable 'org-journal-date-format "%A, %d %B %Y")
;; (require 'org-journal)

;; Set TODO state for region
(defun gack/mark-todo-in-region ()
  (interactive)
  (let ((scope (if mark-active 'region 'tree))
        (state (org-fast-todo-selection)))
    (org-map-entries (lambda () (org-todo state)) nil scope)))

;; Mark as done if children are all done

(defun gack/org-checkbox-todo ()
  "Switch header TODO state to DONE when all checkboxes are ticked, to TODO otherwise"
  (let ((todo-state (org-get-todo-state)) beg end)
    (unless (not todo-state)
      (save-excursion
        (org-back-to-heading t)
        (setq beg (point))
        (end-of-line)
        (setq end (point))
        (goto-char beg)
        (if (re-search-forward "\\[\\([0-9]*%\\)\\]\\|\\[\\([0-9]*\\)/\\([0-9]*\\)\\]"
                               end t)
            (if (match-end 1)
                (if (equal (match-string 1) "100%")
                    (unless (string-equal todo-state "DONE")
                      (org-todo 'done))
                  (unless (string-equal todo-state "TODO")
                    (org-todo 'todo)))
              (if (and (> (match-end 2) (match-beginning 2))
                       (equal (match-string 2) (match-string 3)))
                  (unless (string-equal todo-state "DONE")
                    (org-todo 'done))
                (unless (string-equal todo-state "TODO")
                  (org-todo 'todo)))))))))

(add-hook 'org-checkbox-statistics-hook 'gack/org-checkbox-todo)

;; (defun gack/org-timesheet ()
;;   (nconc
;;    '(("date" "project" "hours" "task"))
;;    '(hline)
;;    (let ((ast (org-element-parse-buffer 'element)))
;;      (org-element-map ast 'clock
;;        (lambda (x)
;;          (let* ((val (org-element-property :value x))
;;                 (task (org-element-property
;;                        :parent
;;                        (org-element-property :parent x))))
;;            `(,(let ((year (org-element-property :year-start val))
;;                     (month (calendar-month-name
;;                             (org-element-property :month-start val)))
;;                     (day (org-element-property :day-start val)))
;;                 ;; (insert (org-element-property :raw-value val))
;;                 (format "%s %s, %s" month day year))
;;              ,(org-element-property :PROJECT task)
;;              ,(org-element-property :duration x)
;;              ,(org-element-property :title task)
;;              )))))
;;    '(hline)
;;    '(("" "total:" ":=vsum(@2..@-1);T" ""))))

(require 'org-ml)
