;;; .doom.d/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here

;;;; Custom functions
;; load my modules
(defvar gack/modules "~/code/dotfiles/razorgrrl/doom-emacs/modules/")
(defun gack/pathmap (in-mod)
  (mapcar 'load
          (mapcar (lambda (x)
                    (format "%s%s" gack/modules x))
                  in-mod)))

(gack/pathmap '("testmod.el"
                "calibreconf.el"
                ;; "common-lisp.el"
                "org.el"
                "elfeed.el"
                "elget.el"
                "guix.el"
                 ;; "quelpa.el"
                "keybinds.el"
                "mu4e.el"
                "dhall.el"
                "comms.el"))

(defun eshell-new()
  "Open a new instance of eshell."
  (interactive)
  (eshell 'N))

;;;; guix workaround https://github.com/alezost/guix.el/issues/39#issuecomment-712413400
(defun guix-buffer-p (&optional buffer)
  (let ((buf-name (buffer-name (or buffer (current-buffer)))))
    (not (null (or (string-match "*Guix REPL" buf-name)
                   (string-match "*Guix Internal REPL" buf-name))))))

(defun guix-geiser--set-project (&optional _impl _prompt)
  (when (and (eq 'guile geiser-impl--implementation)
             (null geiser-repl--project)
             (guix-buffer-p))
    (geiser-repl--set-this-buffer-project 'guix)))

(advice-add 'geiser-impl--set-buffer-implementation :after #'guix-geiser--set-project)

;;;; TRAMP config
(with-eval-after-load 'tramp-sh (push 'tramp-own-remote-path tramp-remote-path))

;;;; evil-mode
(evil-set-undo-system 'undo-fu)

(setq company-tooltip-align-annotations t)

;;;; Rice
;; Custom theme
(add-to-list 'custom-theme-load-path "~/code/dotfiles/razorgrrl/doom-emacs/gack-emacs/theme/")
(setq doom-theme 'doom-gack)

 (defun doom-dashboard-widget-banner ()
   (let ((point (point)))
     (mapc (lambda (line)
             (insert (propertize (+doom-dashboard--center +doom-dashboard--width line)
                                 'face 'doom-dashboard-banner) " ")
             (insert "\n"))
           '(

"                                                                 "
"       ....        .          u           .           ...        "
"    .x88  `^x~  xH(`       us888u.   .udR88N     .xH8%'```'%.    "
"   X888   x8 ` 8888h    .@88  8888  <888'888k   x888~ xnHhx. '.  "
"  88888  888.  %8888    9888  9888  9888 'Y'   X888X 8**8888k `. "
" <8888X X8888   X8?     9888  9888  9888       8888X<~  `8888L ! "
" X8888> 488888>'8888x   9888  9888  9888       88888!   .!8*'' ` "
" X8888>  888888 '8888L  9888  9888  ?8888u../  '88888!'*888x     "
" ?8888X   ?8888>'8888X  '888*'888'  '8888P''   '*8888  8888L     "
"  8888X h  8888 '8888~   ^Y'   ^Y''    'P''    .x.`888X X888X    "
"   ?888  -:8*'  <888'         ███████         '888> %8X !8888..- "
"    `*88.      :88%        █████████████      '888   8  '8888%`  "
"       ^'~====''`        ████   ███   ████       *==      ''     "
"                       ███ ███  ███  ███ ███                     "
"                       ███ █ ██ ███ ██ █ ███                     "
"                       ███ █  ███████  █ ███                     "
"                       ███ █  ███████  █ ███                     "
"                       ███ █ ██ ███ ██ █ ███                     "
"                       ███ ███  ███  ███ ███                     "
"                         ████   ███   ████                       "
"                           █████████████                         "
"                              ███████                            "
"                                                                 "))
     (when (and (display-graphic-p)
                (stringp fancy-splash-image)
                (file-readable-p fancy-splash-image))
       (let ((image (create-image (fancy-splash-image-file))))
         (add-text-properties
          point (point) `(display ,image rear-nonsticky (display)))
         (save-excursion
           (goto-char point)
           (insert (make-string
                    (truncate
                     (max 0 (+ 1 (/ (- +doom-dashboard--width
                                       (car (image-size image nil)))
                                    2))))
                    ? ))))
       (insert (make-string (or (cdr +doom-dashboard-banner-padding) 0)
                            ?\n)))))

(custom-set-faces!
  '(doom-dashboard-banner :foreground "#e60073" :weight bold)
  '(doom-dashboard-menu-title :foreground "#e60073" ))

(set-frame-font "IBM3270" nil t)
(add-to-list 'default-frame-alist '(font . "IBM3270"))
(set-face-attribute 'default nil :height 105)
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))

;; Load private repos
;; (load! "./interaction-log.el/interaction-log.el")

(pdf-loader-install)
(require 'pocket-reader)
(require 'winum)
(winum-mode)
