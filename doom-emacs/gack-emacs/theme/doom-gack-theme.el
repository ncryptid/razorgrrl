;;; doom-gack-theme.el
(require 'doom-themes)

;;
(defgroup doom-gack-theme nil
  "Options for doom-themes"
  :group 'doom-themes)

(defcustom doom-gack-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'doom-gack-theme
  :type 'boolean)

(defcustom doom-gack-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'doom-gack-theme
  :type 'boolean)

(defcustom doom-gack-comment-bg doom-gack-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'doom-gack-theme
  :type 'boolean)

(defcustom doom-gack-padded-modeline nil
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer to
determine the exact padding."
  :group 'doom-gack-theme
  :type '(or integer boolean))

;;
(def-doom-theme doom-gack
  "Doom gack theme"

  ;; name        default   256       16
  ((bg         '("#000000" nil       nil            ))
   (bg-alt     '("#0c0c0c" nil       nil            ))
   (base0      '("#361A29" "#FFD7FF" "black"        ))
   (base1      '("#701543" "#FFD7D7" "brightblack"  ))
   (base2      '("#2C111F" "#FFAFD7" "brightblack"  ))
   (base3      '("#23151C" "#FF87D7" "brightblack"  ))
   (base4      '("#FF73B9" "#FF5FAF" "brightblack"  ))
   (base5      '("#282828" "#FF5FAF" "brightblack"  ))
   (base6      '("#FF3B9D" "#FF5FAF" "brightblack"  ))
   (base7      '("#FF1F8F" "#FF0087" "brightblack"  ))
   (base8      '("#FF0381" "#FF0087" "white"        ))
   (fg-alt     '("#B8005C" "#AF005F" "brightwhite"  ))
   (fg         '("#E60073" "#D7005F" "white"        ))

   (grey       base4)
   (red        '("#A5427E" "#AF5F87" "red"          ))
   (orange     '("#CB5B99" "#D75F87" "brightred"    ))
   (green      '("#7E1172" "#875F87" "green"        ))
   (teal       '("#B271D1" "#AF5FAF" "brightgreen"  ))
   (yellow     '("#EA3994" "#FF87AF" "yellow"       ))
   (blue       '("#D15CFF" "#AF87D7" "brightblue"   ))
   (dark-blue  '("#272031" "#5F5F87" "blue"         ))
   (magenta    '("#A449C8" "#AF5FD7" "magenta"      ))
   (violet     '("#68397B" "#5F5F87" "brightmagenta"))
   (cyan       '("#BD9FFF" "#AFAFFF" "brightcyan"   ))
   (dark-cyan  '("#967BD4" "#8787D7" "cyan"         ))

   ;; face categories -- required for all themes
   (highlight      blue)
   (vertical-bar   (doom-lighten bg 0.05))
   (selection      dark-blue)
   (builtin        blue)
   (comments       (if doom-gack-brighter-comments dark-cyan base5))
   (doc-comments   (doom-lighten (if doom-gack-brighter-comments dark-cyan base5) 0.25))
   (constants      red)
   (functions      yellow)
   (keywords       blue)
   (methods        cyan)
   (operators      blue)
   (type           yellow)
   (strings        teal)
   (variables      cyan)
   (numbers        magenta)
   (region         dark-blue)
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)

   ;; custom categories
   (hidden     `(,(car bg) "black" "black"))
   (-modeline-bright doom-gack-brighter-modeline)
   (-modeline-pad
    (when doom-gack-padded-modeline
      (if (integerp doom-gack-padded-modeline) doom-gack-padded-modeline 4)))

   (modeline-fg     nil)
   (modeline-fg-alt base5)

   (modeline-bg
    (if -modeline-bright
        base3
        `(,(doom-darken (car bg) 0.15) ,@(cdr base0))))
   (modeline-bg-l
    (if -modeline-bright
        base3
        `(,(doom-darken (car bg) 0.1) ,@(cdr base0))))
   (modeline-bg-inactive   (doom-darken bg 0.1))
   (modeline-bg-inactive-l `(,(car bg) ,@(cdr base1))))


  ;; --- extra faces ------------------------
  ((elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")

   ((line-number &override) :foreground fg-alt)
   ((line-number-current-line &override) :foreground fg)
   ((line-number &override) :background (doom-darken bg 0.025))

   (font-lock-comment-face
    :foreground comments
    :background (if doom-gack-comment-bg (doom-lighten bg 0.05)))
   (font-lock-doc-face
    :inherit 'font-lock-comment-face
    :foreground doc-comments)

   (doom-modeline-bar :background (if -modeline-bright modeline-bg highlight))

   (mode-line
    :background modeline-bg :foreground modeline-fg
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
   (mode-line-inactive
    :background modeline-bg-inactive :foreground modeline-fg-alt
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
   (mode-line-emphasis
    :foreground (if -modeline-bright base8 highlight))
   (mode-line-buffer-id
    :foreground highlight)

   (solaire-mode-line-face
    :inherit 'mode-line
    :background modeline-bg-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-l)))
   (solaire-mode-line-inactive-face
    :inherit 'mode-line-inactive
    :background modeline-bg-inactive-l
    :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-l)))

   (telephone-line-accent-active
    :inherit 'mode-line
    :background (doom-lighten bg 0.2))
   (telephone-line-accent-inactive
    :inherit 'mode-line
    :background (doom-lighten bg 0.05))
   (telephone-line-evil-emacs
    :inherit 'mode-line
    :background dark-blue)

   ;; --- major-mode faces -------------------
   ;; css-mode / scss-mode
   (css-proprietary-property :foreground orange)
   (css-property             :foreground green)
   (css-selector             :foreground blue)

   ;; markdown-mode
   (markdown-markup-face :foreground base5)
   (markdown-header-face :inherit 'bold :foreground red)
   (markdown-code-face :background (doom-lighten base3 0.05))

   ;; org-mode
   (org-hide :foreground hidden)
   (org-block :background base2)
   (org-block-begin-line :background base2 :foreground comments)
   (solaire-org-hide-face :foreground hidden))


  ;; --- extra variables ---------------------
  ;; ()
  )

;;; doom-gack-theme.el ends here
